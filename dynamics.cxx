#include "Dynamix.hxx"

#define PI 3.14159265
#define GAMMA_LEN 2

double gz = 9.81;

vec Xdot(GAMMA_LEN * 2);

vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
    

    double frame1g = params["frame1g"];
    double frame1m1 = params["frame1m1"];
    double frame1m2 = params["frame1m2"];
    double frame1r = params["frame1r"];
    double boundary = params["constants_lim"];
    double damping = params["constants_c"];
    double b1 = params["constants_b1"];
    double b2 = params["constants_b2"];
    double k  = params["constants_k"];

    double friction_slide;
    double friction_rotate;
    double spring_force;

    if (X(0) > boundary) {
        spring_force = -k * (X(0) - boundary) - damping * X(2);
    }

    if (X(0) < -boundary) {
        spring_force = -k * (X(0) + boundary) - damping * X(2);
    }

    friction_slide = b1 * X(2);
    friction_rotate = b2 * X(3);

    Xdot(2) = (-frame1r*(frame1m1*frame1r*sin(X(1))*pow(X(3), 2) - (U["F"] - friction_slide + spring_force)) + (frame1g*frame1m1*frame1r*sin(X(1)) + (-friction_rotate))*cos(X(1)))/(frame1r*(frame1m1*pow(sin(X(1)), 2) + frame1m2));
    Xdot(3) = (-frame1m1*frame1r*(frame1m1*frame1r*sin(X(1))*pow(X(3), 2) - (U["F"] - friction_slide + spring_force))*cos(X(1)) + (frame1m1 + frame1m2)*(frame1g*frame1m1*frame1r*sin(X(1)) + (-friction_rotate)))/(frame1m1*pow(frame1r, 2)*(frame1m1*pow(sin(X(1)), 2) + frame1m2));

    // inframe1_rement i and adjust the input velocities
    for (int i=0; i<GAMMA_LEN; i++){
        Xdot(i) = X(GAMMA_LEN + i);
    }

    return Xdot;
}